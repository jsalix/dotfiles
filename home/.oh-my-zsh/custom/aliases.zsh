# Personal aliases

alias salixcode='mosh -p 60001 --ssh="ssh -i ~/.ssh/commander225_id_rsa" root@salixcode.com'
alias salixcode-ssh='ssh -i ~/.ssh/commander225_id_rsa root@salixcode.com'

alias useful='printf "## Useful Commands ##\n* powerpill (pacman wrapper)\n* s-tui\n* nmtui\n* neofetch\n"'
